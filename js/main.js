

$(document).ready(function () {

	// menu
	$(".toggle-menu").click(function(){
		$(this).toggleClass("active")
		$("body").toggleClass("active-menu")
		$(".wrap-menu").toggleClass("active")
	})
	$(document).mouseup(function (e) {
		var container = $("header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$(".toggle-menu").removeClass("active")
				$("body").removeClass("active-menu")
				$(".wrap-menu").removeClass("active")
		}
	});
	// 
	
});


// fix header

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header').addClass('active');
	} else {
		$('#header').removeClass('active')
	};
});





// custome lomo 


$('.slider-banner-main').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 8000,
	autoplayHoverPause: true,
	margin: 0,
	navText: ["<span class='icon icon-prev'></span>","<span class='icon icon-prev'></span>"],
	responsive: {
		0: {
			items: 1,
			loop: ($('.slider-banner-main .item').length > 1) ? true : false,
		},
	}
});
$('.slider-exclusive').owlCarousel({
	nav: true,
	dots: false,
	autoplay: true,
	autoplayTimeout: 8000,
	autoplayHoverPause: true,
	margin: 0,
	navText: ["<img src='./images/i5.png'/>","<img src='./images/i6.png'/>"],
	responsive: {
		0: {
			items: 1,
			loop: ($('.slider-exclusive .item').length > 1) ? true : false,
		},
	}
});
$('.slider-review').owlCarousel({
	nav: false,
	autoplay: true,
	autoplayTimeout: 8000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
			loop: ($('.slider-review .item').length > 1) ? true : false,
		},
	}
});
$('.slider-solution').owlCarousel({
	autoplayHoverPause: true,
	margin: 20,
	navText: ["<i class='fal fa-angle-left'></i>","<i class='fal fa-angle-right'></i>"],
	responsive: {
		0: {
			items: 2,
			nav: true,
			loop: ($('.slider-solution .item').length > 2) ? true : false,
			autoplay: true,
			autoplayTimeout: 1800,
		},
		768: {
			items: 3,
			nav: true,
			loop: ($('.slider-solution .item').length > 3) ? true : false,
			autoplay: true,
			autoplayTimeout: 1800,
		},
		1024: {
			items: 4,
			nav: false,
			loop: ($('.slider-solution .item').length > 4) ? true : false,
			autoplay: false,
		},
	}
});
$('#carouselbenefits').carousel({
	interval: 80000
  })
// 

function resizeImage() {
	let arrClass = [
		{ class: 'resize-image-slider-main', number: (705 / 1900) }, 
		{ class: 'resize-item-solution', number: (123 / 240) }, 
		{ class: 'rezire-exclusive', number: (728 / 1024) }, 
		{ class: 'resize-custommer', number: (162 / 240) }, 
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}

    

	let heiheader = $("header#header").height();
	console.log(heiheader);
	function fixedMenuMobile(n){
		let win = document.getElementById('header').offsetWidth;
		// console.log(win);
		if(win <= 1024){
			$("header#header .wrap-menu").css({"top": n + "px", "height": `calc(100vh - ${n}px)`})
		} else {
			$("header#header .wrap-menu").css({"top": "auto", "height": 'auto'})
		}
	}
resizeImage();
fixedMenuMobile(heiheader);
new ResizeObserver(() => {
	resizeImage();
	fixedMenuMobile(heiheader);
	
}).observe(document.body)

// 

const swiper = new Swiper(".swiper", {
	slidesPerView: 3,
	spaceBetween: 0,
	loop: true,
	grabCursor: true,
	centeredSlides: true,
	slideActiveClass: "active",
	navigation: {
	  nextEl: ".next",
	  prevEl: ".prev"
	},
	pagination: {
	  el: ".pagination",
	  clickable: true
	},
	autoplay: {
	  enabled: true,
	  delay: 50000
	},
	
  });
  
  $('.sticky-nav-tab').click(function () {
	event.preventDefault();
	let id = $(this).attr('href');
	let scrollTop = $(id).offset().top - 70 + 1;
	$('html, body').animate({ scrollTop: scrollTop }, 600);
});